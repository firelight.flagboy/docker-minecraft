#!/usr/bin/env python3

import json
import logging as log
from urllib.request import urlopen, urlretrieve
from argparse import ArgumentParser
from functools import reduce


VERSION_MANIFEST_URL = "https://launchermeta.mojang.com/mc/game/version_manifest.json"
LATEST_RELEASE = "last-release"
LATEST_SNAPSHOT = "last-snapshot"


def parseArgs():
    parser = ArgumentParser()
    parser.add_argument(
        "--version", "-v",
        help="the version of the minecraft server to download, you can use {} or {} to download the latest release / snapshot".format(
            LATEST_RELEASE, LATEST_SNAPSHOT),
        required=False,
        default=LATEST_RELEASE)
    parser.add_argument(
        "--list", "-l", help="list the available version", choices=["release", "snapshot"], default=None)
    parser.add_argument(
        "--out", "-o", help="output destination of the server file", default="server.jar")
    return parser.parse_args()


def loadManifest():
    return json.load(
        urlopen(VERSION_MANIFEST_URL))


def getVersionList(manifest, t):
    """ Generate an array of all available version """
    v = filter(lambda entry: entry["type"] == t, manifest["versions"])
    m = map(lambda entry: entry["id"], v)
    return list(m)


def standardizeVersion(version, manifest):
    """ Standardize LATEST_{RELEASE,SNAPSHOT} into the corresponding version """
    if version == LATEST_RELEASE:
        return manifest["latest"]["release"]
    if version == LATEST_SNAPSHOT:
        return manifest["latest"]["snapshot"]
    return version


def findVersionEntry(version, manifest):
    """ Find the corresponding entry for the version in the manifest """
    v = standardizeVersion(version, manifest)
    versions = manifest["versions"]
    for entry in versions:
        if entry["id"] == v:
            return entry
    raise ValueError(
        "version {} not found in manifest".format(version))


def loadVersionEntry(entry):
    log.debug("version entry: {}".format(entry["url"]))
    return json.load(urlopen(entry["url"]))


def downloadEntry(entry, destination):
    url = entry["downloads"]["server"]["url"]
    log.debug("server download: {}".format(url))
    return urlretrieve(url, destination)


def checkServerChecksum(entry, serverpath):
    import hashlib
    hash_sha1 = hashlib.sha1()
    with open(serverpath, 'rb') as f:
        for chunk in iter(lambda: f.read(4096), b''):
            hash_sha1.update(chunk)
    if entry["downloads"]["server"]["sha1"] != hash_sha1.hexdigest():
        raise ValueError("{} did not match sha1 ( {} != {} )".format(
            serverpath, entry["downloads"]["server"]["sha1"], hash_sha1.hexdigest()))


if __name__ == "__main__":
    log.basicConfig(level=log.DEBUG)
    args = parseArgs()
    manifest = loadManifest()
    if args.list is not None:
        log.info("available {} versions: {}".format(
            args.list, getVersionList(manifest, args.list)))
        exit(0)
    v = standardizeVersion(args.version, manifest)
    log.info("selected version: {}".format(v))
    versionEnt = findVersionEntry(v, manifest)
    log.info("found entry with id {}".format(versionEnt["id"]))
    ent = loadVersionEntry(versionEnt)
    log.info("successfully load version entry")
    log.info("downloading server: {} b".format(
        ent["downloads"]["server"]["size"]))
    downloadEntry(ent, args.out)
    log.info("checking server fingerprint")
    checkServerChecksum(ent, args.out)
    log.info("done, the file was downloaded at {}".format(args.out))
