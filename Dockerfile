FROM alpine

RUN apk update && apk add python3 openjdk8-jre-base

COPY serverdl.py /usr/sbin/.

RUN adduser -S -u 42 minecraft && mkdir -p /home/minecraft/server && chown -R minecraft /home/minecraft
USER minecraft

WORKDIR /home/minecraft/server

COPY --chown=42:42 server.properties .

ARG VERSION=1.16.1
RUN python3 /usr/sbin/serverdl.py -v $VERSION -o server-$VERSION.jar && echo "eula=true" > eula.txt

ENV VERSION ${VERSION}
ENV MIN_MEM 1024M
ENV MAX_MEM 2048M
ENTRYPOINT java -Xms$MIN_MEM -Xmx$MAX_MEM -jar server-$VERSION.jar
